import React,{Component} from 'react'
import { Text, View, TouchableOpacity } from 'react-native'

class SingleView extends Component{
     state = {
        data: []
    }

    componentDidMount = () => {
        fetch('https://vic.uthds4.info/api/productos', { method: 'GET' })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    data: responseJson
                })
            }) 
            .catch((error) => {
                console.error(error);
            });
            
    }
    alertName = (item) => {
        alert(item.nombre)
    }

    render() {
        return (
            
            <View>
            

                {
                    this.state.data.map((item, index)=>(
                        <TouchableOpacity key={item.id}  onPress={() => this.alertName(item)}>
                            <Text  >Id{"\n"} {item.id}</Text>
                            <Text >Nombre{"\n"} {item.nombre}</Text>
                            <Text >Descripción{"\n"} {item.descripcion}</Text>
                            <Text >Precio{"\n"} {item.precio}</Text>
                            <Text >Codigo Barras{"\n"} {item.codBarra}</Text>
                            <Text >Imagen{"\n"} {item.imagen}</Text>
                        </TouchableOpacity>
                    ))
                }
              
                </View>
        )
    }
} 

export default SingleView

