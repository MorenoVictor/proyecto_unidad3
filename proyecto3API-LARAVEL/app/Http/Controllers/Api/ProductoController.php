<?php

namespace App\Http\Controllers\Api;
use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductoController extends Controller
{
    
    public function allProducts(Request $request){
        $products = Producto::all();
        $list = [];
        foreach($products as $product){
            $object = [
                //fields productos table
                'id' =>$product->id,
                'nombre' =>$product->nombre,
                'descripcion' =>$product->descripcion,
                'precio' =>$product->precio,
                'codBarra' =>$product->codBarra,
                'imagen' =>$product->imagen,
            ];
            array_push($list,$object);

        }
        return response()->json($list);
    }

    public function index($id){
        $product = Producto::all()->where('id',$id)->first();

        if (!$product){
            return response()->json(["message"=>"Product not found", "Error"=>true]);
        }

        $object = [
            //fields productos table
            'id' =>$product->id,
                'nombre' =>$product->nombre,
                'descripcion' =>$product->descripcion,
                'precio' =>$product->precio,
                'codBarra' =>$product->codBarra,
                'imagen' =>$product->imagen,
        ];

        return response()->json($object);
    }

}
